package com.josiassena.babylullabies.helpers;

/**
 * File created by josiassena on 12/4/16.
 */
public final class DeveloperURI {

    private DeveloperURI() {
    }

    public static final String URI = "market://search?q=pub:Josias+Sena";
}
