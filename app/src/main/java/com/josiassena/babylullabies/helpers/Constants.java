package com.josiassena.babylullabies.helpers;

import com.josiassena.babylullabies.R;

/**
 * File created by josiassena on 12/4/16.
 */
public class Constants {

    public static final String MAIN_ACTION = "com.josiassena.babylullabies.action.main";
    public static final String INIT_ACTION = "com.josiassena.babylullabies.action.init";
    public static final String PREV_ACTION = "com.josiassena.babylullabies.action.prev";
    public static final String PLAY_ACTION = "com.josiassena.babylullabies.action.play";
    public static final String NEXT_ACTION = "com.josiassena.babylullabies.action.next";
    public static final String STARTFOREGROUND_ACTION = "com.josiassena.babylullabies.action.startforeground";
    public static final String STOPFOREGROUND_ACTION = "com.josiassena.babylullabies.action.stopforeground";

    public static final int NOTIFICATION_ID = 101;

    public static final int[] MAIN_BACKGROUNDS = new int[] {
            R.drawable.main_bg_1,
            R.drawable.main_bg_2,
            R.drawable.main_bg_3,
            R.drawable.main_bg_4,
            R.drawable.main_bg_5,
            R.drawable.main_bg_6};

}