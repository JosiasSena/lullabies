package com.josiassena.babylullabies.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Random;

/**
 * File created by josiassena on 12/27/16.
 */
public class ColorThemeManager {

    private static final String TAG = ColorThemeManager.class.getSimpleName();

    private Context context;
    private OnGotThemeListener onGotThemeListener;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, final Picasso.LoadedFrom from) {
            Log.d(TAG, "onBitmapLoaded() was called");

            setThemeColorsAndImageBackground(bitmap);
        }

        @Override
        public void onBitmapFailed(final Drawable errorDrawable) {
            Log.e(TAG, "onBitmapFailed() was called");
            // do nothing
        }

        @Override
        public void onPrepareLoad(final Drawable placeHolderDrawable) {
            // do nothing
        }
    };

    public ColorThemeManager(@NonNull final Context context,
                             @NonNull final OnGotThemeListener onGotThemeListener) {
        this.context = context;
        this.onGotThemeListener = onGotThemeListener;
    }

    public void getBackgroundImage() {
        final int randomIndex = new Random().nextInt(Constants.MAIN_BACKGROUNDS.length);
        final int background = Constants.MAIN_BACKGROUNDS[randomIndex];

        Picasso.with(context)
                .load(background)
                .noPlaceholder()
                .into(target);
    }

    private void setThemeColorsAndImageBackground(final Bitmap bitmap) {
        Log.d(TAG, "setThemeColorsAndImageBackground() was called");

        Palette.from(bitmap)
                .generate(new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {
                        Log.d(TAG, "onGenerated() was called");

                        Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();

                        if (vibrantSwatch != null) {
                            notifyListenersOFThemeColors(vibrantSwatch);
                        } else {
                            Log.e(TAG, "onGenerated: vibrantSwatch was null. " +
                                    "VibrantSwatch theme colors were not set.");

                            vibrantSwatch = palette.getSwatches().get(0);

                            if (vibrantSwatch != null) {
                                notifyListenersOFThemeColors(vibrantSwatch);
                            } else {

                                notifyListenersOFThemeColors(null);
                            }
                        }

                        final Drawable drawable = new BitmapDrawable(context.getResources(),
                                bitmap);

                        if (onGotThemeListener != null) {
                            onGotThemeListener.onGotBackground(drawable);
                        }
                    }
                });
    }

    private void notifyListenersOFThemeColors(final Palette.Swatch vibrantSwatch) {
        if (onGotThemeListener != null) {
            onGotThemeListener.onGotThemeColors(vibrantSwatch);
        }
    }

    public interface OnGotThemeListener {

        void onGotThemeColors(@Nullable final Palette.Swatch vibrantSwatch);

        void onGotBackground(@NonNull final Drawable drawable);
    }
}
