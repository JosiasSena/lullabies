package com.josiassena.babylullabies.helpers;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * File created by josiassena on 12/26/16.
 */
public final class Utils {

    private Utils() {
        // do nothing
    }

    public static String getSongLengthText(final int duration) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
    }
}
