package com.josiassena.babylullabies.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.josiassena.babylullabies.R;
import com.josiassena.babylullabies.song.SongsManager;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * File created by josiassena on 12/16/16.
 */
public final class TimerDialog {

    private static final String TAG = TimerDialog.class.getSimpleName();
    private static final String[] timerArray = new String[] {"Off", "1 Minute", "2 Minutes",
            "5 Minutes", "10 Minutes", "15 Minutes", "20 Minutes", "25 Minutes", "30 Minutes",
            "35 Minutes", "40 Minutes", "45 Minutes", "1 hour"};

    private static AlertDialog timerDialog;
    private HashMap<String, Long> timers;
    private int selectedScheduleTime = 0;

    public TimerDialog(final Context context) {
        timers = new HashMap<>();

        timers.put("Off", 0L);
        timers.put("1 Minute", TimeUnit.MINUTES.toMillis(1));
        timers.put("2 Minutes", TimeUnit.MINUTES.toMillis(2));
        timers.put("5 Minutes", TimeUnit.MINUTES.toMillis(5));
        timers.put("10 Minutes", TimeUnit.MINUTES.toMillis(10));
        timers.put("15 Minutes", TimeUnit.MINUTES.toMillis(15));
        timers.put("20 Minutes", TimeUnit.MINUTES.toMillis(20));
        timers.put("25 Minutes", TimeUnit.MINUTES.toMillis(25));
        timers.put("30 Minutes", TimeUnit.MINUTES.toMillis(30));
        timers.put("35 Minutes", TimeUnit.MINUTES.toMillis(35));
        timers.put("40 Minutes", TimeUnit.MINUTES.toMillis(40));
        timers.put("45 Minutes", TimeUnit.MINUTES.toMillis(45));
        timers.put("1 Hour", TimeUnit.HOURS.toMillis(1));

        createTimerDialog(context);
    }

    private void createTimerDialog(Context context) {
        timerDialog = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setSingleChoiceItems(timerArray, selectedScheduleTime,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                selectedScheduleTime = which;
                            }
                        })
                .setPositiveButton(R.string.word_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        schedule(selectedScheduleTime);
                    }
                })
                .setNegativeButton(R.string.word_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }
                }).create();
    }

    private void schedule(final int which) {
        Log.d(TAG, "schedule() called with: which = [" + which + "]");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SongsManager.INSTANCE.stopMusicPlayerAndNotifyListeners();
            }
        }, timers.get(timerArray[which]));
    }

    public void show() {
        if (timerDialog != null) {
            timerDialog.show();
        }
    }

    public void hide() {
        if (timerDialog != null) {
            timerDialog.hide();
        }
    }

}
