package com.josiassena.babylullabies.song;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * File created by josiassena on 12/2/16.
 */
@RealmClass
public class Song extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id; // This represents the resource id

    private String name;

    private boolean isFavorite;

    public Song() {
    }

    Song(final String name, final int id) {
        this.name = name;
        this.id = id;
    }

    private Song(Parcel in) {
        name = in.readString();
        id = in.readInt();
        isFavorite = in.readByte() != 0;
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(final boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        parcel.writeString(name);
        parcel.writeInt(id);
        parcel.writeByte((byte) (isFavorite ? 1 : 0));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Song song = (Song) o;
        return id == song.id && isFavorite == song.isFavorite && name.equals(song.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + id;
        result = 31 * result + (isFavorite ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Song {" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", isFavorite=" + isFavorite + " }";
    }
}
