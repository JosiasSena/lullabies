package com.josiassena.babylullabies.song;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.util.Log;

import com.josiassena.babylullabies.R;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.realm.Realm;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * File created by josiassena on 12/2/16.
 */
public enum SongsManager implements MediaPlayer.OnCompletionListener {

    INSTANCE;

    private static final String TAG = SongsManager.class.getSimpleName();

    private List<OnSongPlayingListener> listeners = new ArrayList<>();
    private OnMediaPlayerReadyListener mediaPlayerReadyListener;
    private MediaPlayer mediaPlayer;
    private List<Song> songs;
    private Realm realm;

    private int currentSongIndex = 0;
    private int countToShowAd = 0;

    private boolean isUsingFavorites = false;

    SongsManager() {
        realm = Realm.getDefaultInstance();
    }

    public SongsManager init(@NonNull final Context context,
                             @NonNull final OnMediaPlayerReadyListener mediaPlayerReadyListener) {
        this.mediaPlayerReadyListener = mediaPlayerReadyListener;

        Realm.init(context);

        getAllSongs(context);

        return INSTANCE;
    }

    @Override
    public void onCompletion(final MediaPlayer mediaPlayer) {
        for (OnSongPlayingListener listener : listeners) {
            listener.onSongFinishedPlaying(mediaPlayer);
        }
    }

    public void releaseMusicPlayer() {
        if (mediaPlayer != null) {

            try {
                mediaPlayer.stop();
            } catch (IllegalStateException e) {
                Log.e(TAG, "releaseMusicPlayer() called with: " + e.getMessage(), e);
            }

            mediaPlayer.release();

            mediaPlayer = null;
        }
    }

    public void play(final Context context) {
        Log.d(TAG, "play() was called");

        if (!mediaPlayer.isPlaying()) {
            final Song song = songs.get(currentSongIndex);

            AssetFileDescriptor assetFileDescriptor = getAssetFileDescriptor(context, song);

            try {
                prepareDescriptorAndPlaySong(assetFileDescriptor);

                updateListeners(song);
            } catch (IllegalArgumentException | IOException | IllegalStateException e) {
                Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
            }
        } else {
            mediaPlayer.pause();
        }
    }

    public void playSong(final Context context, final Song song) {
        Log.d(TAG, "playSong() called with: song = [" + song + "]");

        currentSongIndex = songs.indexOf(song);

        AssetFileDescriptor assetFileDescriptor = getAssetFileDescriptor(context, song);

        try {
            prepareDescriptorAndPlaySong(assetFileDescriptor);

            updateListeners(song);
        } catch (IllegalArgumentException | IOException | IllegalStateException e) {
            Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
        }
    }

    public void playPreviousSong(final Context context) {

        if (currentSongIndex == 0) {
            currentSongIndex = songs.size() - 1;
        } else if (currentSongIndex == songs.size() - 1) {
            currentSongIndex = 0;
        } else {
            currentSongIndex--;
        }

        final Song song = songs.get(currentSongIndex);

        AssetFileDescriptor assetFileDescriptor = getAssetFileDescriptor(context, song);

        try {
            prepareDescriptorAndPlaySong(assetFileDescriptor);

            updateListeners(song);
        } catch (IllegalArgumentException | IOException | IllegalStateException e) {
            Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
        }
    }

    public void playNextSong(final Context context) {
        Log.d(TAG, "playNextSong() was called");

        if (currentSongIndex == songs.size() - 1) {
            currentSongIndex = 0;
        } else {
            currentSongIndex++;
        }

        final Song song = songs.get(currentSongIndex);

        AssetFileDescriptor assetFileDescriptor = getAssetFileDescriptor(context, song);

        try {
            prepareDescriptorAndPlaySong(assetFileDescriptor);

            updateListeners(song);
        } catch (IllegalArgumentException | IOException | IllegalStateException e) {
            Log.e(TAG, "Unable to play audio queue do to exception: " + e.getMessage(), e);
        }
    }

    public void stopMusicPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    public void addOnSongPlayingListener(@NonNull OnSongPlayingListener onSongPlayingListener) {

        if (!listeners.contains(onSongPlayingListener)) {
            listeners.add(onSongPlayingListener);
        }
    }

    private void prepareDescriptorAndPlaySong(final AssetFileDescriptor assetFileDescriptor)
            throws IOException {

        mediaPlayer.reset();

        if (assetFileDescriptor != null) {
            mediaPlayer.setDataSource(
                    assetFileDescriptor.getFileDescriptor(),
                    assetFileDescriptor.getStartOffset(),
                    assetFileDescriptor.getDeclaredLength());

            mediaPlayer.prepare();
            mediaPlayer.start();

            assetFileDescriptor.close();
        }

        try {
            mediaPlayer.selectTrack(currentSongIndex);
        } catch (RuntimeException re) {
            Log.e(TAG, "prepareDescriptorAndPlaySong() called with: " + re.getMessage(), re);
        }
    }

    private AssetFileDescriptor getAssetFileDescriptor(final Context context, final Song song) {
        try {
            return context.getResources().openRawResourceFd(song.getId());
        } catch (Resources.NotFoundException ne) {
            Log.e(TAG, "getAssetFileDescriptor() called with: song = [" + song.toString() + "] " +
                    ne.getMessage(), ne);

            deleteAllSongsFromRealm();

            getSongsFromRawResourceFolder(context);
        }

        return null;
    }

    private void deleteAllSongsFromRealm() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                realm.delete(Song.class);
            }
        });
    }

    private void updateListeners(final Song song) {
        countToShowAd++;

        if (countToShowAd % 5 == 0) {
            for (OnSongPlayingListener listener : listeners) {
                listener.onShowAd();
            }
        } else {
            for (OnSongPlayingListener listener : listeners) {
                listener.onPlaySong(song, currentSongIndex);
            }
        }
    }

    public int getCurrentSongIndex() {
        return currentSongIndex;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public List<Song> getSongsFromRealm() {
        if (isUsingFavorites) {
            getFavoriteSongs();
        } else {
            realm.beginTransaction();
            songs = realm.where(Song.class).findAll();
            realm.commitTransaction();
        }

        return songs;
    }

    private void getAllSongs(final Context context) {
        Log.d(TAG, "getAllSongs() was called");

        getSongsFromRealm();

        if (songs == null || songs.isEmpty()) {
            getSongsFromRawResourceFolder(context);
        } else {
            initMediaPlayer(context);
        }
    }

    private void initMediaPlayer(final Context context) {
        Log.d(TAG, "initMediaPlayer() was called");

        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer
                    .create(context, songs.get(currentSongIndex).getId());
        }

        mediaPlayer.setOnCompletionListener(this);

        mediaPlayerReadyListener.onMediaPlayerReady(mediaPlayer);
    }

    private void getSongsFromRawResourceFolder(final Context context) {
        Log.d(TAG, "getSongsFromRawResourceFolder() was called");

        Observable.fromCallable(getSongsCallable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Song>>() {
                    @Override
                    public void onCompleted() {
                        initMediaPlayer(context);
                    }

                    @Override
                    public void onError(final Throwable e) {
                        Log.e(TAG, "onError() called with: " + e.getMessage(), e);
                    }

                    @Override
                    public void onNext(final List<Song> songsReturned) {
                        setSongs(songsReturned);
                    }
                });

//        Field[] fields = R.raw.class.getFields();
//
//        final List<Song> songs = new ArrayList<>();
//
//        for (final Field field : fields) {
//
//            try {
//                Log.e(TAG, "getAllSongs: name = " + getSongName(field) + " id = " +
//                        field.getInt(field));
//                songs.add(new Song(getSongName(field), field.getInt(field)));
//            } catch (IllegalAccessException e) {
//                Log.e(TAG, "getAllSongs() called with: " + "e = [" + e.getMessage() + "]", e);
//            }
//
//        }
//
//        this.songs = songs;
//
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(final Realm realm) {
//                realm.insertOrUpdate(songs);
//            }
//        });
    }

    private void setSongs(final List<Song> songsReturned) {
        Log.d(TAG, "setSongs() was called");

        songs = songsReturned;

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                realm.insertOrUpdate(songs);
            }
        });
    }

    private Callable<List<Song>> getSongsCallable() {
        return new Callable<List<Song>>() {
            @Override
            public List<Song> call() throws Exception {

                final Field[] fields = R.raw.class.getFields();
                final List<Song> songs = new ArrayList<>();

                for (final Field field : fields) {

                    try {
                        songs.add(new Song(getSongName(field), field.getInt(field)));
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "getAllSongs() called with: " +
                                "e = [" + e.getMessage() + "]", e);
                    }
                }

                return songs;
            }
        };
    }

    private String getSongName(final Field field) {

        final String name = field.getName()
                .replace("_", " ");

        StringBuilder res = new StringBuilder();
        String[] wordsArray = name.split(" ");

        for (String word : wordsArray) {
            char[] charArray = word.trim().toCharArray();

            charArray[0] = Character.toUpperCase(charArray[0]);

            word = new String(charArray);

            res.append(word).append(" ");
        }

        return res.toString();
    }

    public List<Song> getFavoriteSongs() {
        realm.beginTransaction();

        songs = realm.where(Song.class)
                .equalTo("isFavorite", true)
                .findAll();

        realm.commitTransaction();

        return songs;
    }

    public void setUsingFavorites(final boolean usingFavorites) {
        this.isUsingFavorites = usingFavorites;
        currentSongIndex = 0;
    }

    public void stopMusicPlayerAndNotifyListeners() {
        Log.d(TAG, "stopMusicPlayerAndNotifyListeners() was called");

        stopMusicPlayer();

        for (OnSongPlayingListener listener : listeners) {
            listener.onStopped();
        }
    }

    public interface OnSongPlayingListener {

        void onPlaySong(final Song song, final int index);

        void onSongFinishedPlaying(MediaPlayer mediaPlayer);

        void onStopped();

        void onShowAd();
    }

    public interface OnMediaPlayerReadyListener {

        void onMediaPlayerReady(final MediaPlayer mediaPlayer);

    }
}
