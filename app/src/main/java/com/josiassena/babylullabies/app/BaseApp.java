package com.josiassena.babylullabies.app;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;
import com.josiassena.babylullabies.R;
import com.josiassena.babylullabies.realm.RealmModules;
import com.josiassena.babylullabies.realm.helpers.RealmConstants;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * File created by josiassena on 12/5/16.
 */
public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MobileAds.initialize(this, getString(R.string.app_id));

        initRealm();
    }

    private void initRealm() {
        Realm.init(this);
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
                .name(RealmConstants.REALM_DB_NAME)
                .schemaVersion(RealmConstants.REALM_DB_SCHEMA_VERSION)
                .deleteRealmIfMigrationNeeded()
                .modules(new RealmModules())
                .build());
    }
}
