package com.josiassena.babylullabies.main.presenter;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.josiassena.babylullabies.main.view.MainView;
import com.josiassena.babylullabies.song.Song;

import java.util.List;

/**
 * File created by josiassena on 12/4/16.
 */
interface MainPresenter extends MvpPresenter<MainView> {

    void init(Context context);

    void startUpdatingSeekBar();

    void unSubscribe();

    void addListeners();

    boolean isSongPlaying();

    void clear();

    void stopUsingFavoriteSongs();

    List<Song> getSongs();

    void startUsingFavoriteSongs();

    boolean isFavoriteSongsAvailable();

    void playSong();

    void pauseSong();

    void playPreviousSong();

    void playNextSong();

    Song getCurrentSong();

    int getCurrentSongDuration();

    int getCurrentSongRemainder();

    void setMusicProgress(int progress);

    String getSongLengthText();

    void getBackgroundImage();
}
