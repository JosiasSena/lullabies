package com.josiassena.babylullabies.main.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.josiassena.babylullabies.R;
import com.josiassena.babylullabies.helpers.Constants;
import com.josiassena.babylullabies.main.view.MainActivity;
import com.josiassena.babylullabies.song.Song;
import com.josiassena.babylullabies.song.SongsManager;

public class NotificationService extends Service implements SongsManager.OnSongPlayingListener {

    private static final String TAG = NotificationService.class.getSimpleName();

    private static final String PAUSE = "Pause";
    private static final String PLAY = "Play";
    private static final String NEXT = "Next";
    private static final String PREVIOUS = "Previous";

    private SongsManager songsManager;

    public NotificationService() {
        songsManager = SongsManager.INSTANCE;
        songsManager.addOnSongPlayingListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() called with: intent = [" + intent + "], flags = [" + flags +
                "], startId = [" + startId + "]");

        if (intent != null) {
            if (intent.getAction().equals(Constants.STARTFOREGROUND_ACTION)) {

                if (isMusicPlaying()) {
                    showNotification(getCurrentSongName(), R.drawable.ic_pause_circle_filled,
                            PAUSE);
                } else {
                    showNotification(getCurrentSongName(), R.drawable.ic_play_circle_filled, PLAY);
                }

            } else if (intent.getAction().equals(Constants.PREV_ACTION)) {
                songsManager.playPreviousSong(this);
            } else if (intent.getAction().equals(Constants.PLAY_ACTION)) {
                songsManager.play(this);

                if (!isMusicPlaying()) {
                    showNotification(getCurrentSongName(), R.drawable.ic_play_circle_filled, PLAY);
                    stopForeground(false);
                }

            } else if (intent.getAction().equals(Constants.NEXT_ACTION)) {
                songsManager.playNextSong(this);
            } else if (intent.getAction().equals(Constants.STOPFOREGROUND_ACTION)) {
                stopForeground(true);
                stopSelf();
            }
        }

        return START_STICKY;
    }

    private boolean isMusicPlaying() {
        return songsManager.getMediaPlayer().isPlaying();
    }

    private String getCurrentSongName() {
        return songsManager.getSongsFromRealm().get(songsManager.getCurrentSongIndex()).getName();
    }

    private void showNotification(String body, int playBtnResId, String playBtnText) {
        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(this);
        notificationCompat.setContentIntent(getContentPendingIntent());
        notificationCompat.setContentTitle(getString(R.string.app_name));
        notificationCompat.setContentText(body);
        notificationCompat.setStyle(getStyle());
        notificationCompat.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        notificationCompat.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        notificationCompat.setSmallIcon(R.drawable.ic_notification);
        notificationCompat
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

        if (Build.VERSION.SDK_INT >= 21) {
            notificationCompat.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        notificationCompat
                .addAction(R.drawable.ic_skip_previous, PREVIOUS, getPreviousPendingIntent());

        notificationCompat.addAction(playBtnResId, playBtnText, getPlayPendingIntent());
        notificationCompat.addAction(R.drawable.ic_skip_next, NEXT, getNextPendingIntent());

        startForeground(Constants.NOTIFICATION_ID, notificationCompat.build());
    }

    private PendingIntent getNextPendingIntent() {
        Intent nextIntent = new Intent(this, NotificationService.class);
        nextIntent.setAction(Constants.NEXT_ACTION);
        return PendingIntent.getService(this, 0, nextIntent, 0);
    }

    private PendingIntent getPlayPendingIntent() {
        Intent playIntent = new Intent(this, NotificationService.class);
        playIntent.setAction(Constants.PLAY_ACTION);
        return PendingIntent.getService(this, 0, playIntent, 0);
    }

    private PendingIntent getPreviousPendingIntent() {
        Intent previousIntent = new Intent(this, NotificationService.class);
        previousIntent.setAction(Constants.PREV_ACTION);
        return PendingIntent.getService(this, 0, previousIntent, 0);
    }

    private PendingIntent getContentPendingIntent() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        return PendingIntent.getActivity(this, 0, notificationIntent, 0);
    }

    @NonNull
    private NotificationCompat.MediaStyle getStyle() {
        return new NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(0, 1, 2);
    }

    @Override
    public void onPlaySong(final Song song, final int index) {
        Log.d(TAG, "onPlaySong() called with: song = [" + song + "], index = [" + index + "]");

        showNotification(song.getName(), R.drawable.ic_pause_circle_filled, PAUSE);
    }

    @Override
    public void onSongFinishedPlaying(final MediaPlayer mediaPlayer) {
        Log.d(TAG, "onSongFinishedPlaying() was called");

        songsManager.playNextSong(this);
    }

    @Override
    public void onStopped() {
        // do nothing
    }

    @Override
    public void onShowAd() {
        // do nothing
    }
}
