package com.josiassena.babylullabies.main.view;

import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.josiassena.babylullabies.song.Song;

/**
 * File created by josiassena on 12/4/16.
 */
public interface MainView extends MvpView {

    void updateUIElements();

    void setSwatchColors(final Palette.Swatch vibrantSwatch);

    void resetAll();

    void onGotSeekBarUpdate(final int currentSongProgress);

    void onSongIsPLaying(final Song song, int duration);

    void onSongFinishedPlaying();

    void displayInterstitialAd();

    void onUpdateProgressBar(int progress);

    void setDefaultUIColors();

    void setMainBackgroundColor(final Drawable drawable);
}
