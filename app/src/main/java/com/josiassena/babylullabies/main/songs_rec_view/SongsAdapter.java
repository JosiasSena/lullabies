package com.josiassena.babylullabies.main.songs_rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.josiassena.babylullabies.R;
import com.josiassena.babylullabies.song.Song;
import com.josiassena.babylullabies.song.SongsManager;

import java.util.List;

import io.realm.Realm;

/**
 * File created by josiassena on 12/3/16.
 */
public class SongsAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Song> songs;
    private Realm realm;
    private SongsManager songsManager;

    public SongsAdapter() {
        realm = Realm.getDefaultInstance();

        this.songs = realm.where(Song.class).findAll();
        songsManager = SongsManager.INSTANCE;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_songs, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Song song = songs.get(position);

        holder.tvSongTitle.setText(song.getName());

        if (song.isFavorite()) {
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite);
        } else {
            holder.ivFavorite.setImageResource(R.drawable.ic_favorite_border);
        }

        holder.ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(final Realm realm) {
                        if (song.isFavorite()) {
                            song.setFavorite(false);
                            holder.ivFavorite.setImageResource(R.drawable.ic_favorite_border);
                        } else {
                            song.setFavorite(true);
                            holder.ivFavorite.setImageResource(R.drawable.ic_favorite);
                        }
                    }
                });
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                songsManager.playSong(holder.itemView.getContext(), song);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (songs != null ? songs.size() : 0);
    }

    public void setSongs(final List<Song> songs) {
        this.songs = songs;
        notifyDataSetChanged();
    }
}
