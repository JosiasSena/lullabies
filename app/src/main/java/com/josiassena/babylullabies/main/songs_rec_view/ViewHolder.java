package com.josiassena.babylullabies.main.songs_rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.josiassena.babylullabies.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * File created by josiassena on 12/3/16.
 */
class ViewHolder extends RecyclerView.ViewHolder {

    @BindView (R.id.tv_song_title)
    TextView tvSongTitle;

    @BindView (R.id.iv_favorite)
    ImageView ivFavorite;

    ViewHolder(final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
