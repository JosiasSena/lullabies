package com.josiassena.babylullabies.main.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.NativeExpressAdView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.josiassena.babylullabies.R;
import com.josiassena.babylullabies.helpers.AdManager;
import com.josiassena.babylullabies.helpers.Constants;
import com.josiassena.babylullabies.helpers.DeveloperURI;
import com.josiassena.babylullabies.helpers.TimerDialog;
import com.josiassena.babylullabies.helpers.Utils;
import com.josiassena.babylullabies.main.notification.NotificationService;
import com.josiassena.babylullabies.main.presenter.MainPresenterImpl;
import com.josiassena.babylullabies.main.songs_rec_view.SongsAdapter;
import com.josiassena.babylullabies.song.Song;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpActivity<MainView, MainPresenterImpl>
        implements MainView, NavigationView.OnNavigationItemSelectedListener,
        SeekBar.OnSeekBarChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    //region View bindings
    @BindView (R.id.toolbar)
    Toolbar toolbar;

    @BindView (R.id.nav_view)
    NavigationView navigationView;

    @BindView (R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView (R.id.song_seek_bar)
    AppCompatSeekBar songSeekBar;

    @BindView (R.id.tv_song_length)
    TextView tvSongLength;

    @BindView (R.id.tv_song_name)
    TextView tvSongName;

    @BindView (R.id.tv_song_remainder)
    TextView tvSongRemainder;

    @BindView (R.id.content_main)
    RelativeLayout contentMain;

    @BindView (R.id.btn_play)
    ImageView playButton;

    @BindView (R.id.rv_songs_list)
    RecyclerView rvSongsList;

    @BindView (R.id.fl_lullaby_bg)
    FrameLayout flLullabyBg;

    @BindView (R.id.ad_view_banner)
    NativeExpressAdView adViewBanner;

    @BindView (R.id.btn_previous)
    ImageButton btnPrevious;

    @BindView (R.id.btn_next)
    ImageButton btnNext;
    //endregion

    private TimerDialog timerDialog;
    private SongsAdapter adapter;
    private AdManager adManager;

    private boolean leaveApp = false;

    @NonNull
    @Override
    public MainPresenterImpl createPresenter() {
        return new MainPresenterImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        initNavDrawer();

        initAds();

        presenter.init(this);

        initSongsRecView();

        timerDialog = new TimerDialog(this);
    }

    private void initAds() {
        Log.d(TAG, "initAds() was called");

        adManager = AdManager.INSTANCE;
        adManager.initAds(this);

        if (adViewBanner != null) {
            adViewBanner.loadAd(adManager.getAdRequest());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        stopMusicPlayerService();

        presenter.addListeners();

        songSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerDialog.hide();
    }

    private void stopMusicPlayerService() {
        stopService(new Intent(MainActivity.this, NotificationService.class));
    }

    private void initSongsRecView() {
        final int orientation = LinearLayoutManager.VERTICAL;
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(orientation);

        rvSongsList.setLayoutManager(layoutManager);

        adapter = new SongsAdapter();

        rvSongsList.setItemViewCacheSize(50);
        rvSongsList.setAdapter(adapter);

        hideSongRecView();
    }

    @Override
    public void updateUIElements() {
        Log.d(TAG, "updateUIElements() was called");

        final int duration = presenter.getCurrentSongDuration();
        final String length = Utils.getSongLengthText(duration);

        songSeekBar.setMax(duration);

        tvSongLength.setText(length);
        tvSongRemainder.setText(length);
        tvSongName.setText(presenter.getCurrentSong().getName());

        if (presenter.isSongPlaying()) {
            playButton.setImageResource(R.drawable.ic_pause_circle_filled);
            songSeekBar.setMax(duration);
            presenter.startUpdatingSeekBar();
        }
    }

    @Override
    public void setDefaultUIColors() {
        toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));

        tvSongLength.setTextColor(Color.WHITE);
        tvSongName.setTextColor(Color.WHITE);
        tvSongRemainder.setTextColor(Color.WHITE);
    }

    @Override
    public void setMainBackgroundColor(final Drawable drawable) {
        contentMain.setBackground(drawable);
    }

    @Override
    public void setSwatchColors(final Palette.Swatch vibrantSwatch) {
        final int mainRGB = vibrantSwatch.getRgb();
        final int bodyTextColor = vibrantSwatch.getBodyTextColor();

        toolbar.setBackgroundColor(mainRGB);

        tvSongLength.setTextColor(bodyTextColor);
        tvSongName.setTextColor(bodyTextColor);
        tvSongRemainder.setTextColor(bodyTextColor);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_timer:
                timerDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {

        stopMusicPlayerService();

        presenter.clear();

        super.onDestroy();
    }

    private void initNavDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_all);
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            closeNavDrawer();
        } else {
            if (rvSongsList.isShown()) {
                rvSongsList.setVisibility(View.GONE);
            } else {
                checkIfUserShouldLeaveApp();
            }
        }
    }

    private void checkIfUserShouldLeaveApp() {
        if (leaveApp) {
            exitApplication();
        } else {
            Toast.makeText(this, R.string.exit_app, Toast.LENGTH_SHORT).show();
            leaveApp = true; // turn exit to true so that we can exit

            // Wait 3 seconds, if the back button is pressed again
            // within 3 seconds then we exit the app
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    leaveApp = false;
                }
            }, 3000);
        }
    }

    public void exitApplication() {
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    @SuppressWarnings ("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        rvSongsList.setVisibility(View.GONE);

        if (id == R.id.nav_all) {
            adManager.displayInterstitialAd();

            playButton.setImageResource(R.drawable.ic_play_circle_filled);

            presenter.stopUsingFavoriteSongs();

            adapter.setSongs(presenter.getSongs());
        } else if (id == R.id.nav_favorites) {

            presenter.startUsingFavoriteSongs();

            if (!presenter.isFavoriteSongsAvailable()) {
                displayNoFavoriteLullabiesAvailableDialog();
            } else {
                updateUIElements();

                adManager.displayInterstitialAd();

                playButton.setImageResource(R.drawable.ic_play_circle_filled);

                adapter.setSongs(presenter.getSongs());
            }
        } else if (id == R.id.nav_share) {
            final Intent msg = new Intent(Intent.ACTION_SEND);
            msg.addCategory(Intent.CATEGORY_DEFAULT);
            msg.putExtra(Intent.EXTRA_SUBJECT, "Best Lullabies application");
            msg.putExtra(Intent.EXTRA_TEXT, "Check out this baby lullaby application! ");
            msg.putExtra(Intent.EXTRA_TITLE, "Baby lullabies");
            msg.setType("text/plain");
            startActivity(Intent.createChooser(msg, "Best Lullabies application"));
        } else if (id == R.id.nav_rate_us) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(intent);
        } else if (id == R.id.nav_more_apps) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(DeveloperURI.URI)));
        }

        closeNavDrawer();
        return true;
    }

    private void displayNoFavoriteLullabiesAvailableDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.no_favs_title)
                .setMessage(R.string.no_favs_body)
                .setCancelable(false)
                .setPositiveButton(R.string.word_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, final int i) {
                        navigationView.setCheckedItem(R.id.nav_all);
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void closeNavDrawer() {
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START, true);
        }
    }

    @OnClick ({R.id.btn_previous, R.id.btn_play, R.id.btn_next, R.id.tv_song_name,
            R.id.fl_lullaby_bg})
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_previous:
                presenter.playPreviousSong();
                break;
            case R.id.btn_play:
                if (rvSongsList.isShown()) {
                    hideSongRecView();
                }

                if (presenter.isSongPlaying()) {
                    presenter.pauseSong();
                    playButton.setImageResource(R.drawable.ic_play_circle_filled);
                } else {
                    playSong();
                }

                break;
            case R.id.btn_next:
                presenter.playNextSong();
                break;
            case R.id.tv_song_name:
                if (rvSongsList.isShown()) {
                    hideSongRecView();
                } else {
                    rvSongsList.setVisibility(View.VISIBLE);

                    Drawable drawable = ContextCompat
                            .getDrawable(this, R.drawable.ic_arrow_drop_up);

                    tvSongName.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                }
                break;
            case R.id.fl_lullaby_bg:
                if (rvSongsList.isShown()) {
                    hideSongRecView();
                }
                break;
        }
    }

    private void hideSongRecView() {
        rvSongsList.setVisibility(View.GONE);

        Drawable drawable = ContextCompat
                .getDrawable(this, R.drawable.ic_arrow_drop_down);

        tvSongName
                .setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
    }

    private void updateRemainderText() {
        int remainder = presenter.getCurrentSongRemainder();

        tvSongRemainder.setText(String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(remainder),
                TimeUnit.MILLISECONDS.toSeconds(remainder) -
                        TimeUnit.MINUTES
                                .toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainder))));
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress,
                                  final boolean fromUser) {

        if (fromUser) {
            presenter.setMusicProgress(progress);
            seekBar.setProgress(progress);

            updateRemainderText();
        }
    }

    @Override
    public void resetAll() {
        Log.d(TAG, "resetAll() was called");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (songSeekBar.getProgress() > 0) {
                    songSeekBar.setProgress(0);
                }

                tvSongRemainder.setText(presenter.getSongLengthText());

                playButton.setImageResource(R.drawable.ic_play_circle_filled);
            }
        });
    }

    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {

    }

    @Override
    public void displayInterstitialAd() {
        adManager.displayInterstitialAd();
    }

    @Override
    public void onUpdateProgressBar(final int progress) {
        if (songSeekBar != null) {
            songSeekBar.setProgress(progress);
        }
    }

    @Override
    public void onSongIsPLaying(final Song song, final int duration) {

        playButton.setImageResource(R.drawable.ic_pause_circle_filled);
        songSeekBar.setMax(duration);

        final String length = Utils.getSongLengthText(duration);

        tvSongLength.setText(length);
        tvSongRemainder.setText(length);
        tvSongName.setText(song.getName());

        presenter.getBackgroundImage();

        hideSongRecView();
    }

    @Override
    public void onSongFinishedPlaying() {
        songSeekBar.setProgress(0);
    }

    private void playSong() {

        final int duration = presenter.getCurrentSongDuration();
        presenter.playSong();

        playButton.setImageResource(R.drawable.ic_pause_circle_filled);

        songSeekBar.setMax(duration);

        presenter.startUpdatingSeekBar();
    }

    @Override
    public void onGotSeekBarUpdate(final int currentSongProgress) {
        songSeekBar.setProgress(currentSongProgress);

        updateRemainderText();
    }

    @Override
    protected void onUserLeaveHint() {

        if (presenter.isSongPlaying()) {
            Intent serviceIntent = new Intent(MainActivity.this, NotificationService.class);
            serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
            startService(serviceIntent);
        }

        super.onUserLeaveHint();
    }
}
