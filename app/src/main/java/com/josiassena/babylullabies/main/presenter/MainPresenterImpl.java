package com.josiassena.babylullabies.main.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.josiassena.babylullabies.helpers.ColorThemeManager;
import com.josiassena.babylullabies.helpers.Utils;
import com.josiassena.babylullabies.main.view.MainView;
import com.josiassena.babylullabies.song.Song;
import com.josiassena.babylullabies.song.SongsManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * File created by josiassena on 12/4/16.
 */
public class MainPresenterImpl extends MvpBasePresenter<MainView> implements MainPresenter,
        SongsManager.OnMediaPlayerReadyListener, SongsManager.OnSongPlayingListener,
        ColorThemeManager.OnGotThemeListener {

    private static final String TAG = MainPresenterImpl.class.getSimpleName();

    private CompositeSubscription compositeSubscription;
    private ColorThemeManager colorThemeManager;
    private SongsManager songsManager;
    private MediaPlayer mediaPlayer;
    private Context context;

    public MainPresenterImpl() {
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void init(final Context context) {
        this.context = context;

        songsManager = SongsManager.INSTANCE;
        songsManager.init(context, this);
        songsManager.addOnSongPlayingListener(this);

        mediaPlayer = songsManager.getMediaPlayer();

        colorThemeManager = new ColorThemeManager(context, this);
    }

    @Override
    public void addListeners() {
        songsManager.addOnSongPlayingListener(this);
    }

    @Override
    public boolean isSongPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    @Override
    public void clear() {
        Log.d(TAG, "clear() was called");

        songsManager.releaseMusicPlayer();
        unSubscribe();
    }

    @Override
    public void startUpdatingSeekBar() {

        compositeSubscription.add(
                Observable.interval(1L, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .filter(new Func1<Long, Boolean>() {
                            @Override
                            public Boolean call(final Long aLong) {
                                return mediaPlayer.isPlaying();
                            }
                        }).subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted() was called");
                        resetAll();
                    }

                    @Override
                    public void onError(final Throwable e) {
                        Log.e(TAG, "onError() called with: " + "e = [" + e.getMessage() + "]", e);
                        resetAll();
                    }

                    @Override
                    public void onNext(final Long interval) {
                        if (getView() != null && isViewAttached()) {
                            getView().onGotSeekBarUpdate(mediaPlayer.getCurrentPosition());
                        }
                    }
                }));
    }

    @Override
    public void unSubscribe() {
        Log.d(TAG, "unSubscribe() was called");

        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.clear();
        }
    }

    @Override
    public void onMediaPlayerReady(final MediaPlayer mediaPlayer) {
        Log.d(TAG, "onMediaPlayerReady() was called");

        this.mediaPlayer = mediaPlayer;

        if (getView() != null && isViewAttached()) {
            getView().updateUIElements();
        }
    }

    @Override
    public void onPlaySong(final Song song, final int index) {
        Log.d(TAG, "onPlaySong() called with: song = [" + song + "], index = [" + index + "]");

        if (mediaPlayer == null) {
            mediaPlayer = songsManager.getMediaPlayer();
        }

        final int duration = mediaPlayer.getDuration();

        mediaPlayer.start();

        if (getView() != null && isViewAttached()) {
            getView().onSongIsPLaying(song, duration);
        }

        startUpdatingSeekBar();
    }

    @Override
    public void onSongFinishedPlaying(final MediaPlayer mediaPlayer) {
        Log.d(TAG, "onSongFinishedPlaying() was called");

        if (getView() != null && isViewAttached()) {
            getView().onSongFinishedPlaying();
        }

        unSubscribe();

        songsManager.playNextSong(context);
    }

    @Override
    public void onStopped() {
        Log.d(TAG, "onStopped() was called");

        songsManager.stopMusicPlayer();

        resetAll();
    }

    @Override
    public void onShowAd() {
        Log.d(TAG, "onShowAd() was called");

        if (getView() != null && isViewAttached()) {
            getView().displayInterstitialAd();
        }
    }

    @Override
    public void startUsingFavoriteSongs() {
        Log.d(TAG, "startUsingFavoriteSongs() was called");

        songsManager.setUsingFavorites(true);

        songsManager.stopMusicPlayer();

        unSubscribe();
    }

    @Override
    public void stopUsingFavoriteSongs() {
        Log.d(TAG, "stopUsingFavoriteSongs() was called");

        songsManager.setUsingFavorites(false);
        songsManager.stopMusicPlayer();
        unSubscribe();

        if (getView() != null && isViewAttached()) {
            getView().updateUIElements();
        }
    }

    @Override
    public List<Song> getSongs() {
        return songsManager.getSongsFromRealm();
    }

    @Override
    public boolean isFavoriteSongsAvailable() {
        return !(songsManager.getSongsFromRealm() == null ||
                songsManager.getSongsFromRealm().isEmpty());
    }

    @Override
    public void playSong() {
        mediaPlayer.start();
    }

    @Override
    public void pauseSong() {
        mediaPlayer.pause();
    }

    @Override
    public void playPreviousSong() {

        resetProgressBar();

        unSubscribe();

        songsManager.playPreviousSong(context);
    }

    @Override
    public void playNextSong() {

        resetProgressBar();

        unSubscribe();

        songsManager.playNextSong(context);
    }

    @Override
    public Song getCurrentSong() {
        return songsManager.getSongsFromRealm().get(songsManager.getCurrentSongIndex());
    }

    @Override
    public int getCurrentSongDuration() {
        return mediaPlayer.getDuration();
    }

    @Override
    public int getCurrentSongRemainder() {
        return mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition();
    }

    @Override
    public void setMusicProgress(final int progress) {
        mediaPlayer.seekTo(progress);
    }

    @Override
    public String getSongLengthText() {
        return Utils.getSongLengthText(mediaPlayer.getDuration());
    }

    @Override
    public void getBackgroundImage() {
        colorThemeManager.getBackgroundImage();
    }

    @Override
    public void onGotThemeColors(@Nullable final Palette.Swatch vibrantSwatch) {

        if (getView() != null && isViewAttached()) {
            if (vibrantSwatch != null) {
                getView().setSwatchColors(vibrantSwatch);
            } else {
                getView().setDefaultUIColors();
            }
        }
    }

    @Override
    public void onGotBackground(@NonNull final Drawable drawable) {
        if (getView() != null && isViewAttached()) {
            getView().setMainBackgroundColor(drawable);
        }
    }

    private void resetAll() {

        songsManager.stopMusicPlayer();

        if (getView() != null && isViewAttached()) {
            getView().resetAll();
        }
    }

    private void resetProgressBar() {
        if (getView() != null && isViewAttached()) {
            getView().onUpdateProgressBar(0);
        }
    }
}
