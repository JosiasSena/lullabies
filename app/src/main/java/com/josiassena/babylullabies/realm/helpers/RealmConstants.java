package com.josiassena.babylullabies.realm.helpers;

/**
 * File created by josiassena on 12/5/16.
 */
public final class RealmConstants {

    private RealmConstants() {

    }

    public static final String REALM_DB_NAME = "lullabies.realm";
    public static final long REALM_DB_SCHEMA_VERSION = 1;
}
